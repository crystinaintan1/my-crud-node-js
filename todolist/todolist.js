const myqsl = require('mysql');
const express = require('express');
var app = express();
const bodyparser = require('body-parser');
app.use(bodyparser.json());

var mysqlConnection = myqsl.createConnection({
    host: 'localhost',
    user:'root',
    password:'12345678',
    database:'to_do_list'
});

mysqlConnection.connect((err)=>{
    if(!err) console.log('DB connection succeded');
    else console.log('DB connection failed \n Error : '+ JSON.stringify(err, undefined, 2));
});

app.listen(7000,()=>console.log('Express server is running at port no : 7000'));

//get All list
app.get('/todolist',(req,res)=>{
    mysqlConnection.query('SELECT * FROM to_do', (err, rows, fields)=>{
        if(!err) res.send(rows);
        else console.log(err);
    })
});

//get any list with id (read to do list)
app.get('/todolist/:id',(req,res)=>{
    mysqlConnection.query('SELECT * FROM to_do where idList = ?',[req.params.id], (err, rows, fields)=>{
        if(!err) res.send(rows);
        else console.log(err);
    })
});

//Delete to do list
app.delete('/todolist/:id',(req,res)=>{
    mysqlConnection.query('DELETE FROM to_do where idList = ?',[req.params.id], (err, rows, fields)=>{
        if(!err) res.send('DELETE SUCCESSFULLY');
        else console.log(err);
    })
});

//insert to do list
app.post('/todolist',(req,res)=>{
    // var tgl = (Date.now()).format('yyyy-mm-dd');
    // let emp = req.body;
    // var sql = "SET @idList = ?; SET @nameList = ?; SET @createdAt = ?; SET @updatedAt = ?;CALL insertProcedure(@idList,@nameList,@createdAt,@updatedAt);";
    var sql = "INSERT INTO to_do (nameList, createdAt) VALUES (?, ?);";
    // mysqlConnection.query(sql,[req.body.idList,req.body.nameList, req.body.createdAt, req.body.updatedAt],(err, rows, fields)=>{
        mysqlConnection.query(sql,[req.body.nameList, req.body.createdAt],(err, rows, fields)=>{
        if(!err) 
        res.send('INSERT SUCCESSFULLY');
        // rows.array.forEach(element => {
        //     if(element.construct == Array)
        //     res.send('Inserted to do list id : '+element[0].idList);
        // });
        else console.log(err);
    })
});

//update to do list
app.put('/todolist',(req,res)=>{
    // var tgl = (Date.now()).format('yyyy-mm-dd');
    var sql = "UPDATE to_do SET nameList = ?, updatedAt = ? WHERE idList = ?";
    mysqlConnection.query(sql,[req.body.nameList, req.body.updatedAt,req.body.idList], (err, rows, fields)=>{
        if(!err) res.send('UPDATE SUCCESSFULLY');
        else console.log(err);
    })
});